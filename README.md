# Docker for Free Pascal Compiler

Lightweight (Alpine based) Docker images for [Free Pascal](https://www.freepascal.org) Compiler with support for Linux and Windows targets (cross compilation).


## Image variants

* By target CPU / OS
  * `x86_64-linux`
  * `x86_64-win64` (cross compile)
* By size
  * `full` \
    Images containing all units.
  * `slim` \
    Images containing the following groups of units only: `fcl-base`, `rtl`, `rtl-console`, `rtl-objpas`.

## Building

### Build images
```console
make builds
```

### Tag images
```console
make tags
```

### Test tagged images
This needs [`bats`](https://github.com/bats-core/bats-core) to be on path.
```console
make tests
```
Alternatively, override `BATS` variable of `make`, i.e.:
```console
make tests BATS=/opt/bats-core/bin/bats
```

### Remove images
```console
make clean
```

### Remove tags
```console
make cleantags
```

## Usage of images

### Show FPC version
```console
docker run --rm signumtemporis/fpc:slim -iV
```

### Compile a file
_`hello.pas`_:
```console
begin
  writeln('Hello world!');
end.
```
```console
docker run --rm -v $(pwd):/workspace signumtemporis/fpc:slim hello.pas
```

### Cross compile to Win64
```console
docker run --rm -v $(pwd):/workspace signumtemporis/fpc:cross.x86_64-win64.slim hello.pas
```

### Extend slim image variant
This allows to add selected units keeping the image small.

***NOTE***: If You don't care about the image size then use the full variant directly.

The example below adds `hash` package.

Build custom image
```console
docker build -t my-fpc - <<<'
ARG VERSION=3.2.2

FROM signumtemporis/fpc:$VERSION-full AS full

FROM signumtemporis/fpc:$VERSION-slim
ARG VERSION
ARG FPC_UNITS_PATH=/usr/local/lib/fpc/$VERSION/units/x86_64-linux
COPY --from=full $FPC_UNITS_PATH/hash $FPC_UNITS_PATH/hash/
'
```

Prapare _`hello-md5.pas`_ file
```console
uses md5;
begin
  writeln(MD5Print(MD5String('Hello world!')));
end.
```

Compile
```console
docker run --rm -v $(pwd):/workspace my-fpc hello-md5.pas
```

If You run the `hello-md5` executable file then it should write `86fb269d190d2c85f6e0468ceca42a20` to the console. \
If You try to compile the same file using base slim image then You will get "*Can't find unit md5 used by Program*" error message.

## License

See the [LICENSE](LICENSE) file.
