ARG ALPINE_TAG

ARG FPC_ARCH=x86_64-linux
ARG FPC_INSTALL_PREFIX=/usr/local

###############################################################################
FROM alpine:$ALPINE_TAG AS download-bin
###############################################################################

ARG FPC_VERSION
ARG FPC_ARCH

WORKDIR /tmp
RUN wget -O fpc-${FPC_VERSION}.${FPC_ARCH}.tar https://downloads.sourceforge.net/project/freepascal/Linux/${FPC_VERSION}/fpc-${FPC_VERSION}.${FPC_ARCH}.tar


###############################################################################
FROM fpc-base
###############################################################################

ARG FPC_VERSION
ARG FPC_ARCH
ARG FPC_INSTALL_PREFIX

RUN set -euxo pipefail; \
    apk add --no-cache gcc make musl-dev; \
    \
    # Workaround musl vs glibc entrypoint for `fpcmkcfg`
    mkdir /lib64; \
    ln -s /lib/ld-musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

WORKDIR /tmp
COPY --from=download-bin /tmp/fpc-${FPC_VERSION}.${FPC_ARCH}.tar .
RUN set -euxo pipefail; \
    tar -xof fpc-${FPC_VERSION}.${FPC_ARCH}.tar; \
    cd fpc-${FPC_VERSION}.${FPC_ARCH}; \
    rm demo* doc*

WORKDIR /tmp/fpc-${FPC_VERSION}.${FPC_ARCH}
RUN set -euxo pipefail; \
    # Workaround for "no same owner" TAR option
    sed -ie 's#^\s*no_same_owner_tar_opt=$#\0-o#' ./install.sh; \
    \
    echo -e $FPC_INSTALL_PREFIX'\nN\nN\nN\n' | sh ./install.sh

WORKDIR /tmp
RUN rm -r /tmp/*

RUN fpc -iV -iTP -iTO
