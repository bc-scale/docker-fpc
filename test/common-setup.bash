#!/usr/bin/env bash

source "$BATS_TEST_DIRNAME"/../settings.sh

_common_setup() {
	DOCKER_RUN_OPTS="--pull never --rm"
}

_common_teardown() {
	rm -f "$BATS_TEST_DIRNAME"/hello{,.exe,.o}
}


_test_reports_version() {
	local VERSION=${BATS_TEST_DESCRIPTION##* }
	run docker run $DOCKER_RUN_OPTS $FPC_IMAGE:latest -iV
	[ "$output" = "$VERSION" ]
}

_test_builds_executable() {
	run docker run $DOCKER_RUN_OPTS -v "$BATS_TEST_DIRNAME":/workspace $FPC_IMAGE:latest hello.pas
	file -b "$BATS_TEST_DIRNAME"/hello$1 | grep -q "$2"
}

_test_builds_linux_executable() {
	_test_builds_executable "" \
		"^ELF 64-bit LSB executable, x86-64,.*, for GNU/Linux\b"
}

_test_builds_windows_executable() {
	_test_builds_executable ".exe" \
		"^PE32+ executable (console) x86-64 .*, for MS Windows$"
}

_test_has_tag() {
	local IMG2=${BATS_TEST_DESCRIPTION##* }

	local ID=$(docker image inspect --format={{.Id}} $FPC_IMAGE:latest)
	local ID2=$(docker image inspect --format={{.Id}} $IMG2)

	[ "$ID" == "$ID2" ]
}
