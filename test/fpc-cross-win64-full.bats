# vim: ft=bash noet

source "$BATS_TEST_DIRNAME/../settings.sh"

FPC_IMAGE=fpc-cross-win64-full

setup() {
	load 'common-setup'
	_common_setup
}

teardown() {
	_common_teardown
}


@test "[$FPC_IMAGE] reports version $FPC_VERSION" {
	_test_reports_version
}

@test "[$FPC_IMAGE] builds x86-64 Windows executable" {
	_test_builds_windows_executable
}

@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:cross.x86_64-win64" {
	_test_has_tag
}
@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:$FPC_VERSION-cross.x86_64-win64" {
	_test_has_tag
}
@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:cross.x86_64-win64.full" {
	_test_has_tag
}
@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:$FPC_VERSION-cross.x86_64-win64.full" {
	_test_has_tag
}
