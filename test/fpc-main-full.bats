# vim: ft=bash noet

source "$BATS_TEST_DIRNAME/../settings.sh"

FPC_IMAGE=fpc-main-full

setup() {
	load 'common-setup'
	_common_setup
}

teardown() {
	_common_teardown
}


@test "[$FPC_IMAGE] reports version $FPC_VERSION" {
	_test_reports_version
}

@test "[$FPC_IMAGE] builds x86-64 Linux executable" {
	_test_builds_linux_executable
}

@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:latest" {
	_test_has_tag
}
@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:$FPC_VERSION" {
	_test_has_tag
}
@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:full" {
	_test_has_tag
}
@test "[$FPC_IMAGE] has tag $PUBLIC_NAME:$FPC_VERSION-full" {
	_test_has_tag
}
